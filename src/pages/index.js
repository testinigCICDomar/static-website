import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Traficant says hi" />
    <h1>Hi Omar</h1>
    <h2>This new title for merge req testing</h2>
    <p>Welcome to your new Gatsby site.</p>
    <p>hi Aahdi, Hello Arrouma.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
    <div>Version: %%VERSION%%</div>
  </Layout>
)

export default IndexPage
