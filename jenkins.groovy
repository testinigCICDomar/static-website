job('build') {
    description("build stage ")
    
    steps {
		shell('sudo gem install jekyll')
		shell('jekyll new omar-site --force')
		shell('cd omar-site/')
		shell('pwd')
		shell('bundle exec jekyll serve &')
    }
    publishers {
        //archive the war file generated
        archiveArtifacts('public/*.*')
    }

}